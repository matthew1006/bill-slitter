var BottomEdge = (function () {

    function BottomEdge (element) {

        this.elm = document.getElementById(element);
        this.tab = this.elm.querySelectorAll('.tab')[0];
        this.closeButton = this.elm.querySelectorAll('[data-role="bottom-edge-close"]')[0];

        this.style = window.getComputedStyle(this.elm, null);

        //this.elm.style.top = window.innerHeight + "px";
        this.offset_data = 0;

        this.dragStart = function (event)
        {
            this.offset_data = parseInt(this.style.getPropertyValue("top"),10) - event.touches[0].clientY;
            this.elm.style["-webkit-transition-duration"] = "0s";
        }

        this.dragListItem = function (event)
        {
            var top = (event.touches[0].clientY + this.offset_data);
            var height = parseInt(this.style.height,10);

            if (top > window.innerHeight) top = window.innerHeight;
            else if (top < window.innerHeight - height) top = window.innerHeight - height;

            this.elm.style.top = top + 'px';
        }

        this.dropListItem = function (event)
        {
            var top = parseInt(this.style.top,10);
            var height = parseInt(this.style.height,10);
            this.elm.style["-webkit-transition-duration"] = "0.3s";

            if (top <= window.innerHeight - 70) {
                this.elm.style.top = (window.innerHeight - height) + "px";
            }
            else {
                this.elm.style.top = window.innerHeight + "px";
            }
        }

        var self = this;

        this.tab.addEventListener ("touchstart", function (e) { self.dragStart (e); });
        this.tab.addEventListener ("touchend", function (e) { self.dropListItem (e); });
        this.tab.addEventListener ("touchmove", function (e) { self.dragListItem (e); });

        if (this.closeButton) {
            this.closeButton.addEventListener ("click", function(e) { self.close(); });
        }
    }

    BottomEdge.prototype = {
        close: function () {
            this.elm.style.top = window.innerHeight + "px";
        }
    }

    return BottomEdge;
})();
