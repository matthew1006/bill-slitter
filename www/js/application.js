function Application(UIContext) {
    this._uiContextClass = UIContext;
    this._initialized = false;
};

var UI = null;
var personIdCounter = 0;
var currentPerson = null;
var people = [];
var windowDimensions = {};
var tip = 1.1;

Application.prototype.init = function() {
    if (this._uiContextClass && !this._initialized) {
        this._initialized = true;
        UI = new this._uiContextClass();
        UI.init();

        UI.sections = function(id) {
            if (typeof Sections != 'undefined' && Sections) {
                return new Sections(id);
            } else {
                console.error('Could not find the Sections element. You might be missing the "sections.js" Sections definition script. Please add a <script> declaration to include it.');
            }
        }

        UI.bottomEdge = function(id) {
            if (typeof BottomEdge != 'undefined' && BottomEdge) {
                return new BottomEdge(id);
            } else {
                console.error('Could not find the BottomEdge element. You might be missing the "bottom-edge.js" BottomEdge definition script. Please add a <script> declaration to include it.');
            }
        }

        var bottomEdge = UI.bottomEdge ("bottom-edge");

        UI.button("add-items-button").click (addNewItemToList);
        UI.button("add-people-button").click (addNewPersonToList);

        var sections = UI.sections("person-selector");
        sections.onClicked (setCurrentPerson);

        windowDimensions = {
            "width" : window.outerWidth,
            "height": window.outerHeight
        };

        initialiseCurrency (UI);
    }
};

Application.prototype.initialized = function() {
    return this._initialized;
};

function setCurrentPerson (name) {
    for (var i in people) {
        if (people[i].name === name.values) {
            clearItemList();
            currentPerson = people[i];
            populateItemList();
        }
    }
}

function addNewPersonToList () {

    var id = personIdCounter++;

    var newPerson = {
        "name" : "#" + id,
        "total" : function () {
            var t = 0;
            for (var i in this.items) {
                t += this.items[i].value;
            }
            return t;
        },
        "items" : [],
        "colour" : "#1ab6ef",
        "id" : id.toString(),
        "elm" : null
    };

    var list = UI.list ('[id="people-list"]');
    var person = createSwipable (list, personTemplate);

    newPerson.elm = person.elm;

    var colourSelector = person.elm.querySelector ("[id=colour]");
    colourSelector.addEventListener ("click", function () { showColourPicker (id) });

    var nameEntry = person.elm.querySelector ("[id=name-entry]");
    nameEntry.addEventListener ("change", function () { setPersonName (id, nameEntry.value) });

    var deleteButton = person.elm.querySelector ("[id=delete-button]");
    deleteButton.addEventListener ("click", function () { removePerson (id) });

    UI.sections ("person-selector").add (newPerson.name);

    people.push(newPerson);

    drawChart();
}

// People
function removePerson (personId) {
    for (var i in people) {
        if (personId == people[i].id) {

            if (currentPerson === people[i]) {
                currentPerson = null;
                clearItemList();
            }

            var item = people[i].elm;
            while (item.getAttribute ("data-role") !== "swipeable") {
                item = item.parentNode;
            }
            item = item.parentNode;
            item.parentNode.removeChild (item);
            people.splice (i, 1);
            UI.sections ("person-selector").remove (i);
            drawChart ();
            break;
        }
    }
}

function showColourPicker (personId) {
    var dialog = UI.dialog("colour-picker-dialog");
    dialog.show ();

    var colourPicker = UI.optionselector ("colour-picker", true);

    var callback = function (e) {
        for (var i in people) {
            if (personId == people[i].id) {
                people[i].colour = e.values;
                people[i].elm.querySelector('[id="colour"]').style.background = e.values;

                colourPicker.optionselector_ul.removeEventListener ("onclicked", callback);

                drawChart();
                break;
            }
        }
        dialog.hide ();
    };

    colourPicker.onClicked (callback);
}

function setPersonName (personId, name) {

    for (var i in people) {
        if (personId == people[i].id) {
            people[i].name = name;
            UI.sections ("person-selector").setValue (i, name);
            drawChart();
            break;
        }
    }
}
//End

//Items
function populateItemList () {
    if (currentPerson) {
        for (var i in currentPerson.items) {
            addItemToList (currentPerson.items[i], i);
        }
    }
}

function clearItemList () {

    var list = UI.list ('[id="item-list"]');
    list.removeAllItems ();
    drawChart();
}

function addNewItemToList () {
    if (currentPerson) {
        var newItem = {
            "name": "Item",
            "value": 0.00
        };

        addItemToList (newItem, currentPerson.items.length);
        currentPerson.items.push (newItem);
    }
}

function copyItemInList (item) {
    var newItem = {
        "name": item.name,
        "value": item.value
    };

    addItemToList (newItem, currentPerson.items.length);
    currentPerson.items.push (newItem);
    drawChart();
}

function validateMoneyInput (value) {
    value = parseFloat (value);
    if (value < 0) value = 0;
    return value;
}

function addItemToList (item, index) {

    var list = UI.list ('[id="item-list"]');
    var temp = createSwipable (list, foodItemTemplate);

    var deleteButton = temp.elm.querySelector ('[id=delete-button]');
    deleteButton.addEventListener ("click", function () { removeItemFromList(index, temp.elm); }.bind(temp));

    var copyButton = temp.elm.querySelector ('[id=copy-button]');
    copyButton.addEventListener ("click", function () { copyItemInList(item); }.bind(item));

    var nameInput = temp.elm.querySelector ('[id=item-name]');
    nameInput.setAttribute ("value", item.name);
    nameInput.addEventListener ("change", function () {
        item.name = nameInput.value;
        drawChart();
    });

    var valueInput = temp.elm.querySelector ('[id=item-value]');
    valueInput.setAttribute ("value", item.value);
    valueInput.setAttribute ("step", currentCurrency.step);
    valueInput.addEventListener ("change", function () {
        item.value = validateMoneyInput(valueInput.value);
        valueInput.value = item.value.toFixed(currentCurrency.decimals);
        drawChart();
    });

    var symbol = temp.elm.querySelector ("[data-type=currency-symbol]");
    symbol.innerHTML = currentCurrency.html;
}

function removeItemFromList (index, element) {

    while (element.getAttribute ("data-role") !== "swipeable") {
            element = item.parentNode;
    }
    element = element.parentNode;
    element.parentNode.removeChild (element);
    currentPerson.items.splice (index, 1);
    drawChart();
}
//End

//Chart
function drawChart () {

    var canvas = document.getElementById ("chart-canvas");
    canvas.width = windowDimensions.width;
    canvas.height = windowDimensions.height - 150;

    var context = canvas.getContext("2d");

    var center = {"x": canvas.width * 0.5, "y": canvas.height * 0.5};
    var radius = Math.min (canvas.width, canvas.height) * 0.25;

    var total = 0;

    for (var i in people) {
        total += people[i].total();
    }

    var startAngle = -0.5 * Math.PI;

    if (total > 0)
    {
        for (var i in people) {

            var personTotal = people[i].total();
            if (personTotal > 0)
            {

                context.beginPath ();
                context.moveTo (center.x, center.y);

                var endAngle = startAngle + personTotal * 2 * Math.PI / total;

                context.arc(center.x, center.y, radius, startAngle, endAngle, false);
                context.closePath();

                context.fillStyle = people[i].colour;
                context.fill();

                if (personTotal / total >= 0.01)
                {
                    drawChartLable (canvas, context, people[i], (endAngle + startAngle) * 0.5, radius, center);
                }

                startAngle = endAngle;
            }
        }
    }
    context.beginPath ();
    context.arc(center.x, center.y, radius * 0.8, 0, 2 * Math.PI, false);
    context.closePath();

    context.fillStyle = "#f5f5f5";
    context.fill();

    var fontSize = 36 * radius / 150;
    context.font = fontSize + "pt Ubuntu";
    context.fillStyle = "#5d5d5d";
    context.textAlign = "center";
    context.fillText (toCurrencyValue(total), center.x, center.y + fontSize * 0.5);

    context.save();
}

function drawChartLable (canvas, context, person, angle, radius, center) {

    var coords = {
        "x" : center.x + Math.cos(angle) * radius * 1.2,
        "y" : center.y + Math.sin(angle) * radius * 1.2
    };

    if (angle < -Math.PI * 0.5 || angle > Math.PI * 0.5) {
        context.textAlign = "right";
    }
    else {
        context.textAlign = "left";
    }
    context.font = 22 * radius / 150 + "pt Ubuntu";

    context.fillText (formatName(person.name, 8, true, true), coords.x, coords.y);

    context.font = 16 * radius / 150 + "pt Ubuntu";
    context.fillText (toCurrencyValue(person.total()), coords.x, coords.y + 20);
}

function toCurrencyValue (value) {
    return currentCurrency.symbol + (value * tip).toFixed(currentCurrency.decimals);
}

function formatName (name, maxLength, elipses, splitAtSpace) {
    var addElipses = false;

    if (splitAtSpace) {
        var spit = name.split(" ");
        if (spit.length > 1) {
            name = spit[0];
            addElipses = true;
        }
    }

    if (name.length > maxLength) {
        name = name.substring (0, maxLength);
        addElipses = true;
    }

    if (elipses && addElipses) {
        name += "\u2026";
    }

    return name;
}

//End

function setTip (value) {

    tip = value;

    var label = document.getElementById ("tip-label");
    if (label) {
        label.innerHTML = Math.round((tip - 1) * 100) + "%";
    }

    var slider = document.getElementById ("tip-slider");
    if (slider) {
        slider.value = tip;
    }

    drawChart ();
}
