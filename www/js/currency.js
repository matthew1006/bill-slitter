var currentCurrency = null;

var currencies = {
    "USD" : {
        "name":"Dollar",
        "symbol":"$",
        "html":"&#36;&nbsp;",
        "step":0.01,
        "decimals": 2,
        "defaultTip":1.2
    },
    "EUR" : {
        "name":"Euro",
        "symbol":"€",
        "html":"&euro;&nbsp;",
        "step":0.01,
        "decimals": 2,
        "defaultTip":1.05
    },
    "GBP" : {
        "name":"Pound",
        "symbol":"\u00A3",
        "html":"&pound;&nbsp;",
        "step":0.01,
        "decimals": 2,
        "defaultTip":1.1
    },
    "CHF" : {
        "name":"Franc",
        "symbol":"Fr",
        "html":"Fr&nbsp;",
        "step":0.01,
        "decimals": 2,
        "defaultTip":1
    },
    "RUB" : {
        "name":"рубль",
        "symbol":"\u20BD",
        "html":"&#x20BD;&nbsp;",
        "step":0.01,
        "decimals": 2,
        "defaultTip":1
    },
    "SEK" : {
        "name":"Krona",
        "symbol":"kr",
        "html":"kr&nbsp;",
        "step":0.01,
        "decimals": 2,
        "defaultTip":1
    },
    "JPY" : {
        "name":"\u5186",
        "symbol":"\u00A5",
        "html":"&yen;&nbsp;",
        "step":0,
        "decimals": 0,
        "defaultTip":1
    },
    "CNY" : {
        "name":"人民币",
        "symbol":"\u00A5",
        "html":"&yen;&nbsp;",
        "step":0.01,
        "decimals": 2,
        "defaultTip":1
    }
};

function initialiseCurrency (UI) {

    setCurrency (localStorage.getItem ("currency"));

    var currencyLists = document.querySelectorAll("[data-role=option-selector].currency-selector");

    for (var i in currencyLists) {
        if (currencyLists[i].id) {
            var os = UI.optionselector (currencyLists[i].id, false);
            os.optionselector_ul.innerHTML = "";

            for (var id in currencies) {
                var li = document.createElement ("li");
                addClickListener (os, li, id);

                var div1 = document.createElement("div");
                var div2 = document.createElement("div");

                div1.style = "display:inline-block; width:50px";
                div2.style = "display:inline";

                div1.innerHTML = currencies[id].symbol;
                div2.innerHTML = currencies[id].name;

                li.appendChild (div1);
                li.appendChild (div2);

                os.optionselector_ul.appendChild (li);
            }

            os.optionselector_ul_li = os.optionselector.querySelectorAll('li')
        }
    }
}

function addClickListener (os, li, id) {
    li.addEventListener ("click", function (e) {
        os.__onClicked(li, e)
    }, false);

    li.addEventListener ("click", function (e) { setCurrency (id) });
}

function setCurrency (id) {

    if (!(id && id in currencies)) id = "USD";
    localStorage.setItem ("currency", id);

    currentCurrency = currencies[id];

    setTip (currentCurrency.defaultTip);

    var htmlInstances = document.querySelectorAll ("[data-type=currency-symbol]");

    for (var i in htmlInstances) {
        htmlInstances[i].innerHTML = currentCurrency.html;
    }

    htmlInstances = document.querySelectorAll ("[data-type=currency-value]");

    for (var i in htmlInstances) {
        htmlInstances[i].step = currentCurrency.step;
        htmlInstances[i].value = parseFloat(htmlInstances[i].value).toFixed (currentCurrency.decimal);
    }

    drawChart ();
}
