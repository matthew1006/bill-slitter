var Swipeable = (function () {

    /*
    var elm;
    var style;
    var startLeft = 0;
    var minLeft = 0;
    var maxLeft = 0;
    var offset_data = 0;
    */
    function Swipeable (element) {

        this.elm = element;
        this.style = window.getComputedStyle(this.elm, null);

        var nodes = this.elm.querySelectorAll('li');
        var widthTotal = 0;
        this.startLeft = 0;

        for (var i = 0; i < nodes.length; i++)
        {
            var nodeStyle = window.getComputedStyle(nodes[i],null);
            var nodeWidth = parseInt(nodeStyle.getPropertyValue("width"),10);
            var nodePadding = parseInt(nodeStyle.getPropertyValue("padding-left"),10);

            if (nodes[i].className.indexOf ("middle") > -1)
            {
                this.startLeft = -widthTotal + nodePadding;
            }
            else
            {
                widthTotal += nodeWidth + nodePadding;
            }
        }

        this.minLeft = 0;
        this.maxLeft = -widthTotal + nodePadding;
        this.elm.style.left = this.startLeft + "px";
        this.offset_data = 0;

        this.dragStart = function (event)
        {
            this.offset_data = parseInt(this.style.getPropertyValue("left"),10) - event.touches[0].clientX;
            this.elm.style["-webkit-transition-duration"] = "0s";
        }

        this.dragListItem = function (event)
        {
            var left = (event.touches[0].clientX + this.offset_data);

            if (left < this.maxLeft) left = this.maxLeft;
            else if (left > this.minLeft) left = this.minLift;

            this.elm.style.left = left + 'px';
        }

        this.dropListItem = function (event)
        {
            var left = parseInt(this.style.left,10);
            this.elm.style["-webkit-transition-duration"] = "0.2s";

            if (left > this.minLeft + (this.startLeft * 0.5)) {
                this.elm.style.left = this.minLeft + "px";
            }
            else if (left < this.maxLeft - (this.startLeft * 0.5)) {
                this.elm.style.left = this.maxLeft + "px";
            }
            else {
                this.elm.style.left = this.startLeft + "px";
            }
        }
    }


    return Swipeable;
})();

var swipeables = document.querySelectorAll ('[data-role]');

function addCallbackHandler(temp) {
    temp.elm.addEventListener ("touchstart", function (e) { temp.dragStart (e); });
    temp.elm.addEventListener ("touchend", function (e) { temp.dropListItem (e); });
    temp.elm.addEventListener ("touchmove", function (e) { temp.dragListItem (e); });
}

function createSwipable (parent, template) {
    var li = document.createElement("li");
    var div = document.createElement("div");
    var list = document.createElement("ul");

    for (var part in template) {
        addChild (list, template[part])
    }

    div.appendChild (list);
    div.setAttribute ("data-role", "swipeable");
    li.appendChild(div);

    parent._list.querySelector('ul').appendChild(li);

    var temp = new Swipeable(div);
    addCallbackHandler (temp);
    return temp;
}

function addChild (parent, template) {

    if (typeof template === "string") {
        parent.innerHTML = template;
    }
    else {
        var child = document.createElement (template.tag);

        for (var attribute in template) {
            switch (attribute) {
            case "tag":
                break;
            case "children":
                for (var i in template.children) {
                    addChild (child, template.children[i]);
                }
                break;
            default:
                child.setAttribute (attribute, template[attribute]);
                break;
            }
        }

        parent.appendChild (child);
    }
}

for (var  i = 0; i < swipeables.length; i++) {
    if (swipeables[i].getAttribute("data-role") === "swipeable") {
         addCallbackHandler(new Swipeable (swipeables[i]));
    }
}
