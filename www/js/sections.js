/*
 Copyright (C) 2016 Matthew Edwards <geohash@outlook.com>

 This file is part of geohash.

 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 3 of the
 License, or (at your option) any later version.

 This package is distributed WITHOUT ANY WARRANTY;
 without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 See the GNU General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this program. If not, see
 <http://www.gnu.org/licenses/>.
*/

var Sections = (function () {

    var  __values = "";

    function Sections (id) {
        this.sections = document.getElementById(id);

        if (this.sections === null) {
            console.error('The Sections with the ID #' + this.id + ' doesn\'t exist');
            return;
        }

        this.sections_ul = this.sections.querySelectorAll('ul')[0];
        if (this.sections_ul === null || this.sections_ul.length === 0)
            return;

        this.sections_ul_li = this.sections_ul.querySelectorAll('li');
        if (this.sections_ul_li === null || this.sections_ul_li.length === 0)
            return;

        [].forEach.call(this.sections_ul_li, function (elm) {
            elm.addEventListener('click', this.__onClicked.bind(this, elm), false);
        }.bind(this));
    }

    Sections.prototype = {
        __onClicked: function (elm, e) {
            __values = "";

            [].forEach.call(this.sections_ul_li, function (elm) {
                elm.classList.remove('active');
            });
            elm.classList.add('active');

            k = 0;
            for (i = 0, max = this.sections_ul_li.length; i < max; i++) {
                var li = this.sections_ul_li[i];
                if ((li.className).indexOf('active') > -1) {
                    if (k === 0) {
                        __values = li.getAttribute("data-value");
                    } else {
                        __values = __values + ", " + li.getAttribute("data-value");
                    }
                    k++;
                }
            }

            this.__ClickEvent(elm);
            e.preventDefault();
        },

        /**
         * @private
         */
        __ClickEvent: function (elm) {
            elm._evt = document.createEvent('Event');
            elm._evt.initEvent('onclicked', true, true);
            elm._evt.values = __values;
            elm.dispatchEvent(elm._evt);
        },

        onClicked : function(callback){
            this.sections_ul.addEventListener("onclicked", callback);
        },

        add : function (text) {
            var li = document.createElement("li");
            li.setAttribute ("data-value", text);
            li.innerHTML = text;

            li.addEventListener('click', this.__onClicked.bind(this, li), false);

            this.sections_ul.appendChild (li);
            this.sections_ul_li = this.sections_ul.querySelectorAll('li');
        },

        setValue : function (index, text) {
            var li = this.sections_ul_li[index];

            if (li)
            {
                li.setAttribute("data-value", text);
                li.innerHTML = text;
            }
        },

        remove : function (index) {
            this.sections_ul.removeChild (this.sections_ul_li[index]);
            this.sections_ul_li = this.sections_ul.querySelectorAll('li');
        },

        removeAll : function () {
            while (this.sections_ul.hasChileNodes ()) {
                this.sections_ul.removeChild (this.sections_ul.childNodes.lastChild);
            }
            this.sections_ul_li = this.sections_ul.querySelectorAll('li');
        }
    };
    return Sections;
})();
