var foodItemTemplate = [
	{
        "tag": "li",
		"style": "width:120px; background:#fc4949; color:#f5f5f5",
		"children": [
			{
				"tag": "img",
                "src": "/usr/share/icons/ubuntu-mobile/actions/scalable/delete.svg"
			}
		],
        "id": "delete-button"
	},
	{
        "tag": "li",
		"class": "middle",
		"children": [
			{
				"tag": "div",
				"children": [
					{
						"tag": "input",
						"type": "text",
						"class": "labelInput",
                        "style": "width:45%; float:left",
                        "placeholder": "Item",
                        "id": "item-name"
					},
					{
						"tag": "span",
                        "style": "float:right; text-align:right; width:45%; font-size:16pt",
						"children": [
                            {
                                "tag": "normal",
                                "data-type": "currency-symbol",
                                "children": ["&pound;&nbsp;"]
                            },
							{
								"tag": "input",
								"type": "number",
								"value": "1.00",
								"class": "labelInput",
								"style": "width:100px; text-align:right",
                                "step": "0.01",
                                "id": "item-value",
                                "min": 0,
                                "data-type": "currency-value",
							}
						]			
					}
				]
				
			}
		]
    },
    {
        "tag": "li",
        "id": "copy-button",
        "children": [
            {
                "tag": "img",
                "src": "/usr/share/icons/suru/actions/scalable/edit-copy.svg"
            }
        ],

    }
];

var personTemplate = [
    {
        "tag": "li",
        "style": "width:120px; background:#fc4949; color:#f5f5f5",
        "id": "delete-button",
        "children": [
            {
                "tag": "img",
                "src": "/usr/share/icons/ubuntu-mobile/actions/scalable/delete.svg"
            }
        ]
    },
    {
        "tag": "li",
        "class": "middle",
        "children": [
            {
                "tag": "div",
                "children": [
                    {
                        "tag": "input",
                        "type": "text",
                        "class": "labelInput",
                        "placeholder": "Name",
                        "id": "name-entry",
                        "style": "width: calc(100% - 3rem); margin-right:5px"
                    },
                    {
                        "tag": "div",
                        "id" : "colour",
                        "data-role": "shape",
                        "style": "background:#1ab6ef; height:2rem; width:2rem"
                    }
                ]
            }
        ]
    }
];
